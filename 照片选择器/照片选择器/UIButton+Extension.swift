//
//  UIButton+Extension.swift
//  照片选择器
//
//  Created by waterfoxjie on 15/8/17.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

extension UIButton {

    convenience init(imgName : String) {
    
        self.init()
        
        setImage(imgName)
        
    }
    
    func setImage(imgName : String) {
    
        setImage(UIImage(named: imgName), forState: UIControlState.Normal)
        
        setImage(UIImage(named: imgName + "_highlighted"), forState: UIControlState.Highlighted)
        
    }
    
}
